﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Operacio
    {
        private int n;
        public void SumaArreglo()
        {
            int total;
            Console.WriteLine("Dame el tamaño del arreglo: ");
            n = int.Parse(Console.ReadLine());
            int[] arreglo = new int[n];
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("dame numero: ");
                arreglo[i] = int.Parse(Console.ReadLine());
            }
            total = arreglo.Sum();
            Console.WriteLine("Suma de tus numeros: " + total);
            Console.ReadKey();
        }
        public void ImprimePares()
        {

            Console.WriteLine("Dame el tamaño del arreglo: ");
            n = int.Parse(Console.ReadLine());
            int[] arreglo = new int[n];
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("dame numero: ");
                arreglo[i] = int.Parse(Console.ReadLine());
            }
            for (int i = 0; i < n; i++)
            {
                Console.Write(arreglo[i] + " ");
            }
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                if (arreglo[i] % 2 == 0)
                {

                    Console.WriteLine(arreglo[i] + " Es par");
                }

            }
        }
    }
    }
