﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication4
{
    public class Operacion
    {
        public int n;
        public int m;
        public int[,] matriz = new int[2, 2];
        public void leer()
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Console.Write("Dame valor de: [" + i + "," + j + "]=");
                    matriz[i, j] = int.Parse(Console.ReadLine());
                }
            }
        }

        public void imprimir()
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2   ; j++)
                {
                    Console.Write(" " + matriz[i, j]);
                }
                Console.WriteLine();
            }
        }
    }
}