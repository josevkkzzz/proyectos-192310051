﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mascotas 
{
    class Program
    {
        class Mascota
        {
            private String color;
            private String raza;
            private String tipo;
            private double peso;
            private int años;

            public void inicializar()
            {
                Console.WriteLine("color de tu mascota");
                color = Console.ReadLine();
                Console.WriteLine("raza de tu mascota");
                raza = Console.ReadLine();
                tipo = OptenerTipoMascota();
                Console.WriteLine("cual es el peso?");
                peso = double.Parse(Console.ReadLine());
                Console.WriteLine("cuantos años tiene?");
                años = int.Parse(Console.ReadLine());

            }
            private string OptenerTipoMascota()
            {
                string respuesta;
                Console.WriteLine("elije una opcion de mascota");
                Console.WriteLine("1).Perro");
                Console.WriteLine("2).Gato");
                Console.WriteLine("3).serpiente");
                Console.WriteLine("4).hamster");

                int capturaConsole = int.Parse(Console.ReadLine());


                switch (capturaConsole)
                {
                    case 1:
                        respuesta = "perro";
                        break;
                    case 2:
                        respuesta = "gato";
                        break;
                    case 3:
                        respuesta = "serpiente";
                        break;
                    case 4:
                        respuesta = "hamster";
                        break;
                    default:
                        respuesta = "otra mascota";
                        break;
            }

                return respuesta;

            }


        }
    
        static void Main(string[] args)
        {
            Mascota o = new Mascota();
            o.inicializar();

            Console.ReadKey();   
        }
    }
}
